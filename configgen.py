# -*- coding: utf8 -*-

import numpy as np


def genReglar():
    # gen all configs
    ms = np.linspace(0, 0.5, 51)
    rs = np.linspace(0, 0.2, 41)

    counter = 0
    filenames = []
    for p in [0.03, 0.05]:
        for degree in [2, 3]:
            for m in ms:
                for r in rs:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=300\n")
                        f.write("int agent_n=80\n")
                        f.write("int steps=400\n")
                        f.write("int degree=%d\n" % degree)
                        f.write("double r=%f\n" % r)
                        f.write("double m=%f\n" % m)
                        f.write("double p=%f\n" % p)
                        counter += 1
                        filenames.append(filename)

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4Regular():
    # gen all configs
    ms = np.linspace(0, 0.5, 51)
    rs = np.linspace(0, 0.6, 51)
    #ks = np.linspace(0, 0.3, 51)

    counter = 0
    for degree in [3, 4, 6]:
        for m in ms:
            for r in rs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=300\n")
                    f.write("int degree=%d\n" % degree)
                    f.write("double r=%f\n" % r)
                    f.write("double h=0.1\n")
                    f.write("double m=%f\n" % m)
                    f.write("int repeat=100\n")
                    f.write("double times=150.\n")
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4RegularNew():
    size = 300
    degree = 6
    h = 1.
    repeat = 1000
    times = 20000.
    # gen all configs
    mus = np.linspace(0, 0.25, 126)
    #ks = np.linspace(0, 0.4, 81)
    ks = np.linspace(0.4, 1.0, 121)

    counter = 0
    for mu in mus:
        for k in ks:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % mu)
                f.write("double k=%f\n" % k)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genTree():
    # gen all configs
    mus = np.linspace(0, 10, 51)
    ks = np.linspace(0, 0.3, 51)

    counter = 0
    for sons, level in [(2, 8), (3, 5), (4, 4)]:
        for mu in mus:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int sons=%d\n" % sons)
                    f.write("int level=%d\n" % level)

                    f.write("double mu=%f\n" % mu)
                    f.write("double k=%f\n" % k)

                    f.write("int repeat=100\n")
                    f.write("double times=150.\n")
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFast():
    # gen all configs
    mus = [5, 8, 10, 15]
    ks = np.linspace(0.005, 0.25, 101)

    counter = 0
    for size in [8, 10, 20, 30, 40, 60, 80, 100]:
        for mu in mus:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=3\n")

                    f.write("double mu=%f\n" % mu)
                    f.write("double k=%f\n" % k)
                    f.write("double h=0.05\n")

                    f.write("int repeat=100\n")
                    f.write("double times=2000.\n")
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFastEr1():
    # gen all configs
    ks = np.linspace(0.001, 0.01, 91)
    degrees = range(3, 15)

    counter = 0
    for degree in degrees:
        for k in ks:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % 300)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % 10)
                f.write("double k=%f\n" % k)
                f.write("double h=0.08\n")

                f.write("int repeat=200\n")
                f.write("double times=2000.\n")
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFastEr2():
    # gen all configs
    ks = np.linspace(0.001, 0.01, 91)
    #degrees = [3, 4, 6, 8]
    degree = 4
    sizes = [150, 200, 250, 300, 350, 400, 450]

    counter = 0
    for size in sizes:
        for k in ks:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % 10)
                f.write("double k=%f\n" % k)
                f.write("double h=0.08\n")

                f.write("int repeat=200\n")
                f.write("double times=2000.\n")
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowEr():
    size = 1000
    degree = 8

    mu = 0.01
    h = 1.
    repeat = 3000
    times = 2000.
    #mus = np.linspace(0, 0.25, 126)
    mus = [0.05, 0.1, 0.15, 0.2]
    ks = np.linspace(0.02, 0.15, 131)

    counter = 0
    for mu in mus:
        for k in ks:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % mu)
                f.write("double k=%f\n" % k)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)

if __name__ == "__main__":
    genSlowEr()

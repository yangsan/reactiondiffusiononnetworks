#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
//
    int size = parser.getInt("size");
    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");
    int degree = parser.getInt("degree");
    double r = parser.getDouble("r");
    double m = parser.getDouble("m");
    double p = parser.getDouble("p");

    ofstream out;

    // small world
    PUNGraph graph = TSnap::GenSmallWorld(size, degree, p);


    out.open(mylib::tapeFileName("./data/smallworld/", ".txt", argv[1]));
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# k=" << degree << endl;
    out << "# p=" << p << endl;

    for(double & k: mylib::linspace(0, 0.3, 50))
    {
        ConstDiffuse cd(agent_n, r, k, m, graph);
        out << k << " ";
        cd.sim(steps, out);
    }
    out.close();


    return 0;
}

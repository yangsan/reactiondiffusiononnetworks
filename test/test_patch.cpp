#include <gtest/gtest.h>
#include "patch.h"

TEST(Test_PatchBase, Test_Initialization)
{
    int N(1000);
    double f0(0.2), r(0.1);
    PatchBase pb(N, f0, r);

    EXPECT_EQ(pb.getNumber(), static_cast<int>(N*f0));
    EXPECT_DOUBLE_EQ(pb.birth_rate(), r);
    EXPECT_DOUBLE_EQ(pb.death_rate(), r);
    EXPECT_DOUBLE_EQ(pb.getPercent(), f0);

    pb.oneMCStep();
    pb.reset();
    EXPECT_DOUBLE_EQ(pb.getPercent(), f0);

    double f1(0.1);
    pb.setF0(f1);
    EXPECT_DOUBLE_EQ(pb.getPercent(), f1);
}

TEST(Test_PatchBase, Test_Pop_Push)
{
    int N(1000);
    double f0(0.2), r(0.1);
    PatchBase pb(N, f0, r);

    EXPECT_EQ(pb.getNumber(), 200);
    for(int i=0; i<200; ++i)
    {
        int code = pb.popOne();
        EXPECT_EQ(code, 1);
        EXPECT_EQ(pb.getNumber(), 199-i);
    }
    int code = pb.popOne();
    EXPECT_EQ(code, 0);
    EXPECT_EQ(pb.getNumber(), 0);
    code = pb.popOne();
    EXPECT_EQ(pb.getNumber(), 0);
    
    for(int i=0; i<200; ++i)
    {
        pb.pushOne();
        EXPECT_EQ(pb.getNumber(), i+1);
    }
}

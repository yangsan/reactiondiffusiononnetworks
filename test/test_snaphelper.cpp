#include <vector>
#include <gtest/gtest.h>
#include "snaphelper.h"
#include "Snap.h"

using namespace std;

TEST(Test_Network, Test_All)
{
    int number(1000), degree(4);
    PUNGraph graph;
    graph = TSnap::GenRndDegK(number, degree);
    Network<int> net(graph);
    EXPECT_EQ(number, net.node_number);

    int counter(0);
    for(Network<int>::NodeIterator iter = net.begin(); iter != net.end(); iter++)
    {
        EXPECT_EQ(counter, iter->GetId());
        iter.getNode() = iter->GetId();
        ++counter;
    }

    for(Network<int>::NodeIterator iter = net.begin(); iter != net.end(); iter++)
    {
        EXPECT_EQ(iter->GetId(), iter.getNode());
    }

    for(int i=0; i<number; ++i)
    {
        EXPECT_EQ(net.getNodeFromId(i)->GetId(), i);
    }

}

TEST(Test_one_d_gen, Test_All)
{
    int N(100);
    TUNGraph::TNodeI node_iter;

    PUNGraph graph = genOneDLattice(N);
    EXPECT_EQ(graph->GetNodes(), N);

    for(int i=0; i<N; ++i)
    {
        node_iter = graph->GetNI(i);
        EXPECT_EQ(node_iter.GetId(), i);

        if(i==0)
        {
            EXPECT_EQ(node_iter.GetDeg(), 1);
            EXPECT_EQ(node_iter.GetNbrNId(0), 1);
        }
        else if(i==N-1)
        {
            EXPECT_EQ(node_iter.GetDeg(), 1);
            EXPECT_EQ(node_iter.GetNbrNId(0), i-1);
        }
        else
        {
            EXPECT_EQ(node_iter.GetDeg(), 2);
            EXPECT_TRUE(node_iter.IsNbrNId(i-1));
            EXPECT_TRUE(node_iter.IsNbrNId(i+1));
        }
    }

    graph = genOneDLattice(N, true);

    for(int i=0; i<N; ++i)
    {
        node_iter = graph->GetNI(i);
        EXPECT_EQ(node_iter.GetId(), i);

        if(i==0)
        {
            EXPECT_EQ(node_iter.GetDeg(), 2);
            EXPECT_TRUE(node_iter.IsNbrNId(N-1));
            EXPECT_TRUE(node_iter.IsNbrNId(i+1));
        }
        else if(i==N-1)
        {
            EXPECT_EQ(node_iter.GetDeg(), 2);
            EXPECT_TRUE(node_iter.IsNbrNId(i-1));
            EXPECT_TRUE(node_iter.IsNbrNId(0));
        }
        else
        {
            EXPECT_EQ(node_iter.GetDeg(), 2);
            EXPECT_TRUE(node_iter.IsNbrNId(i-1));
            EXPECT_TRUE(node_iter.IsNbrNId(i+1));
        }
    }
}

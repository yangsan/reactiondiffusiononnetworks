#ifndef ALLEEDIFFUSION_H
#define ALLEEDIFFUSION_H

#include "snaphelper.h"
#include "patch.h"
#include <iostream>
#include "rand.h"

class AlleeDiffusionBase
{
    public:
        typedef Network<AlleePatch> NetType;
    protected:
        int patch_capa;
        double growth_rate;
        double allee_threshold;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::RandInt rand_agent;
        mylib::RandDouble randd;
    public:
        AlleeDiffusionBase(int n, double r, double k, PUNGraph graph):
            patch_capa(n),
            growth_rate(r),
            allee_threshold(k),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            rand_agent(1, patch_capa),
            randd()
        {
        }

        virtual void diffuse() = 0;

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatch & patch = node_iter.getNode();
                patch.setCapa(patch_capa);
                patch.setGrowthRate(growth_rate);
                patch.setAlleeK(allee_threshold);
                patch.setF0(0);
            }
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
//                writeDist(out);
                out << getOcupition() << endl;
                if(verbose)
                    cout << i << " steps.." << endl;
            }
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa*net_size; ++i)
            {
                oneTimeStep();
            }
        }
        
        virtual void oneTimeStep()
        {
            birthDeath();
            diffuse();
        }

        virtual void birthDeath()
        {
            // choose a patch randomly
            net.getNodeFromId(rand_patch.gen()).getNode().birthDeathProcess();
        }

        virtual void writeDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter.getNode().getPercent() << " ";
            out << endl;
        }

        virtual void getDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter->GetId() << " " << node_iter.getNode().getPercent() << endl;
        }

        virtual double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().getPercent();
            return base;
        }

};

class ConstDiffuse: public AlleeDiffusionBase
{
    protected:
        double diffusion_rate;
    public:
        ConstDiffuse(int n, double r, double k, double m, PUNGraph graph):
            AlleeDiffusionBase(n, r, k, graph),
            diffusion_rate(m)
        {
            initialize();
        }

        virtual void initialize()
        {
            AlleeDiffusionBase::initialize();
            net.getNodeFromId(rand_patch.gen()).getNode().setF0(1);
        }

        virtual void diffuse()
        {
            // choose a node randomly
            NetType::NodeIterator node_iter = net.getNodeFromId(rand_patch.gen());
            // get the node itself
            AlleePatch & patch = node_iter.getNode();

            // choose an agent from the patch randomly
            if(rand_agent.gen()<= patch.getNumber())
            {
                // decide if to diffuse
                if(randd.gen() < diffusion_rate)
                {
                    // diffuse
                    patch.popOne();
                    // choose a neighor to go
                    int number_of_neighbor = node_iter->GetDeg();
                    if(number_of_neighbor>0)
                    {
                        net.getNodeFromId(
                                node_iter->GetNbrNId(rand_patch.genOTF(0, number_of_neighbor-1))
                                ).getNode().pushOne();
                    }
                }
            }
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
//            out << net_size << " "
//                << patch_capa << " "
//                << growth_rate << " "
//                << allee_threshold << " "
//                << diffusion_rate << " ";
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
//                writeDist(out);
                out << getOcupition() << " ";
                if(verbose)
                    cout << i << " steps.." << endl;
            }
            out << endl;
        }

};

// class to numerically solve the equation sets
class AlleeRK4
{
    public:
        typedef Network<AlleePatchRK4> NetType;

        double allee_threshold;
        double mu; // diffusion_rate / growth_rate
        double interval;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4(double k, double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            allee_threshold(k),
            mu(mu_ratio),
            interval(h),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().h = interval;
        }

        void seed()
        {
            getRandomNode().u = 1;
        }

        void seed(int id)
        {
            getNode(id).u = 1;
        }

        AlleePatchRK4 & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        AlleePatchRK4 & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                double ocupition = getOcupition();
                out << ocupition << " ";
                if(verbose)
                    std::cout << i * interval << std::endl;
                if(swc.pushValue(ocupition))
                    break;
            }
            out << endl;
        }

        void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                if(swc.pushValue(getOcupition()))
                    break;
            }
        }

        void dist(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                    out << node_iter.getNode().u << " ";
                out << endl;
                oneTimeStep();
            }
        }

        void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
        }

        void updatek1s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.k1 = patch.u * (1-patch.u) * (patch.u - allee_threshold) - mu * patch.u + mu * neighbor_flow;
            }
        }

        void updatek2s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k1 / 2.;
                patch.k2 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek3s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k2 / 2.;
                patch.k3 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek4s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k3;
                patch.k4 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;
            }
        }

        void updateus()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        bool guard()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().u;
            return base;
        }
};
#endif

#ifndef WAVE_H
#define WAVE_H

#include "snaphelper.h"
#include "patch.h"
#include <iostream>
#include "rand.h"

class AlleeWaveSys
{
    typedef Network<AlleePatch> NetType;
    private:
        int patch_capa;
        double growth_rate;
        double allee_threshold;
        double diffusion_rate;
        int patch_number;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::RandInt rand_agent;
        mylib::RandDouble randd;
    public:
        AlleeWaveSys(int n, double r, double k, double m, int length):patch_capa(n), growth_rate(r), allee_threshold(k), diffusion_rate(m),  patch_number(length), net(genOneDLattice(length)), rand_patch(0, patch_number-1), rand_agent(1, patch_capa), randd()
        {
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatch & patch = node_iter.getNode();
                patch.setCapa(patch_capa);
                patch.setGrowthRate(growth_rate);
                patch.setAlleeK(allee_threshold);

                if(node_iter->GetId() < patch_number / 2)
                    patch.setF0(1);
                else
                    patch.setF0(0);
            }
        }

        void sim(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
                writeDist(out);
            }
        }

        void oneMCStep()
        {
            for(int i=0; i<patch_capa*patch_number; ++i)
            {
                oneTimeStep();
            }
        }
        
        void oneTimeStep()
        {
            birthDeath();
            diffuse();
        }

        void birthDeath()
        {
            // choose a patch randomly
            net.getNodeFromId(rand_patch.gen()).getNode().birthDeathProcess();
        }

        void diffuse()
        {
            NetType::NodeIterator node_iter = net.getNodeFromId(rand_patch.gen());
            AlleePatch & patch = node_iter.getNode();
            if(rand_agent.gen()<= patch.getNumber())
            {
                if(randd.gen() < diffusion_rate)
                {
                    patch.popOne();
                    if(node_iter->GetDeg()==1)
                    {
                        net.getNodeFromId(node_iter->GetNbrNId(0)).getNode().pushOne();
                    }
                    else
                    {
                        if(randd.gen() < 0.5)
                            net.getNodeFromId(node_iter->GetNbrNId(0)).getNode().pushOne();
                        else
                            net.getNodeFromId(node_iter->GetNbrNId(1)).getNode().pushOne();
                    }
                }
            }
        }

        void writeDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter.getNode().getPercent() << " ";
            out << endl;
        }

        void getDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter->GetId() << " " << node_iter.getNode().getPercent() << endl;
        }
};

#endif

#ifndef PATCH_H
#define PATCH_H

#include "rand.h"
#include <iostream>

using namespace std;

class PatchBase
{
    protected:
        int patch_capa;
        int number;
        double initial_percent;
        double growth_rate;
        mylib::RandDouble randd;
        mylib::RandInt randint;
    public:
        PatchBase(int N, double f0, double r): patch_capa(N), number(N*f0), initial_percent(f0), growth_rate(r), randd(), randint(1, patch_capa) {}

        virtual double birth_rate() { return growth_rate; }
        virtual double death_rate() { return growth_rate; }
        virtual double getPercent() { return number / static_cast<double>(patch_capa); }
        virtual int getNumber() { return number; }

        virtual void reset() { number = static_cast<int>(patch_capa * initial_percent); }
        virtual void setF0(double f0) { initial_percent = f0; reset(); }
        virtual void setCapa(int N) { patch_capa=N; }
        virtual void setGrowthRate(double r) { growth_rate=r; }

        virtual int popOne()
        {
            if(number==0)
                return 0;

            --number;
            return 1;
        }
        virtual void pushOne() { ++number; }

        virtual int birthDeathProcess()
        {
            if(randint.gen() <= number)
            {
                double r = randd.gen();
                if(r < birth_rate())
                {
                    ++number;
                }
                else if(r < birth_rate() + death_rate())
                {
                    --number;
                }
            }
            return 1;
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa; ++i)
                birthDeathProcess();
        }

        virtual void sim(int steps, ostream& out)
        {
            reset();
            for(int i=0; i<steps; ++i)
            {
                out << getPercent() << endl;
                oneMCStep();
            }
        }
};

class AlleePatch: public PatchBase
{
    protected:
        double allee_threshold;
    public:
        AlleePatch(): PatchBase(1, 0, 0), allee_threshold(0) {}
        AlleePatch(int N, double f0, double r, double k):PatchBase(N, f0, r), allee_threshold(k) {}

        virtual void setAlleeK(double k) {allee_threshold=k;}

        virtual double birth_rate()
        {
            return growth_rate * getPercent() * (1+allee_threshold);
        }
        virtual double death_rate()
        {
            double percent = getPercent();
            return growth_rate * (percent * percent + allee_threshold) ;
        }
};

class AlleePatchRK4
{
    public:
        double u; // the value
        double k1, k2, k3, k4; // rk4
        double h;

        AlleePatchRK4(): u(0.), k1(0.), k2(0.), k3(0.), k4(0.), h(0.) {}
        AlleePatchRK4(double u0, double interval): u(u0), k1(0.), k2(0), k3(0), k4(0), h(interval) {}

        double updateValue()
        {
            u = u + h * (k1 + 2*k2 + 2*k3 + k4) / 6.;
            return u;
        }
};


#endif

#include<iostream>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<memory.h>
#include<fstream>
#include<vector>
#include<time.h>
#include<algorithm>
#include"random.cpp"

using namespace std;

const int m0=4;
const int n0=100;
const int fre=1;/////内部接触率
const float re=0.01;/////康复率
const int A=2;//////对于度越大的节点其边伸出的距离越大为A*k^{0.5}这样的分布
const float lambda=3.0;/////度分布服从幂律分布

#define seedNumber 25
#define Length 50
#define Higth 50
#define Round 10000

class Node
{
    public:
        int k,emer;
        vector<int> group;//用于动态存储所包含的成员,应该是个马尔可夫过程
        vector<int> chain;
        vector<int> free;
        vector<vector<int> >  wide;
        vector<int> nodeIndex;
        vector<int> edgeIndex;
        float *mp,*s;
};
class Host
{
    public:
        int state;
        int label;
};

class Edge
{
    public:
        int edgeIndexfrom;
        int edgeIndexto;
};

class Net
{
    private:
        int nodeNumber;
        int edgeNumber;
        unsigned long member_number;
        Node *node;
        Host *member;

    public:
        void netBuilder(int seed);
        void Spread(int r1, float rate, float v);//rate为感染率v某一个城市和从人口的迁移比率或迁移人口的比例）
        void inState1();
        void inState(int seed1);
        int rand_select(int small1, int big1);
        void swap(int *arry1, int a1, int a2);
        void shuffle(int *arry2, int len);

        int max;
        vector<int>::iterator nindex;
        vector<int>::iterator nindex2;
        int *number;
        long double *iptime;
        long double *nodepro;//具有特定度的每个城市的感染率
        long double *infectpro;//感染城市所占的比例（有一个感染者就是感染的城市）
        long double ipro,infect;///参数最后的数值依赖于感染率和n0的大小
        long double *nodek;
        int *stat;
        long double ipmax,infectmax;
        int judge;
        int round;
};

/////////////////////////////////////////////////调用一个完整流程的类（同时将数值输出出来）///////////////////////////////////////////
class Ensemble
{
    public:
        Net *net;
        void Build();
        void Print();
};
///////////////////////////////////////////////////////////////////////////////
void Ensemble::Build()
{
    int i,j;
    net=new Net[seedNumber];
    for(i=0;i<seedNumber;i++)
    {
        j=i/5+1;
        net[i].netBuilder(j);
    }
}
/////建立网络的过程//可以建立不同的网络结构只是点和边的度和邻居发生了变化，在演化的过程中边和节点的总数保持不变，所以这node和edge的储存还是使用数组比较好。
////从small1和big1之间随机选择一个整数
int Net::rand_select(int small1, int big1)
{
    return genrand64_int64()%(big1-small1+1)+small1;
}
////////对与数组中两个序号为a1和a2的数组元素进行交换
void Net::swap(int *arry1, int a1, int a2)
{
    arry1[a1]+=arry1[a2]; 
    arry1[a2]=arry1[a1]-arry1[a2]; 
    arry1[a1]=arry1[a1]-arry1[a2]; 
}
///////////对整个数组的元素两两交换
void Net::shuffle(int *arry2, int len)
{
    int i,j;
    for(int i=0; i<len;i++){ 
        //和后面的随机交换就可以了 
        swap(arry2, i, rand_select(i,len-1)); 
    } 
}

void Net::netBuilder(int seed)
{
    int i,j,f,t,n,h;
    int distence,kd;
    int kmin,kmax;
    long edgeNumber1;
    vector<int> left;
    Edge *edge;
    init_genrand64(seed);

    nodeNumber=Length*Higth;
    node=new Node[nodeNumber];
    number=new int[nodeNumber];

    ///////////////////////////首先对每个点分配一个服从幂律分布的度，然后确定总的变数
    kmin=m0; kmax=m0*(int(pow(nodeNumber,1/(lambda-1))));
    for(i=0;i<nodeNumber;i++) node[i].k=0;///进行初始化

    i=j=0;
    while(i<nodeNumber){
        j=int(pow(genrand64_real2(),1.0/(1.0-lambda)));
        if(kmin<=j && j<=kmax) node[i++].k=j;
    }	

    kd=0;	
    for(i=0;i<nodeNumber;i++)
    {kd+=node[i].k;}

    edgeNumber1=kd/2;
    edge=new Edge[edgeNumber1];
    kd=0;
    ////////首先构造一个二维方格基底，然后在这个基础上进行连接最近邻，使其达到其度的要求
    for(i=0;i<Higth;i++)
        for(j=0;j<Length-1;j++)
        {
            f=i*Length+j;
            t=i*Length+j+1;
            edge[kd].edgeIndexfrom=f;	
            edge[kd].edgeIndexto=t;
            node[f].nodeIndex.push_back(t);
            node[t].nodeIndex.push_back(f);
            kd++;
        }

    for(j=0;j<Length-1;j++)
        for(i=0;i<Higth;i++)
        {
            f=j*Higth+i;
            t=(j+1)*Higth+i;
            edge[kd].edgeIndexfrom=f;	
            edge[kd].edgeIndexto=t;
            node[f].nodeIndex.push_back(t);
            node[t].nodeIndex.push_back(f);
            kd++;
        }

    for(i=0;i<Higth;i++)
    {
        f=i*Length+Length-1;
        t=i*Length;
        edge[kd].edgeIndexfrom=f;	
        edge[kd].edgeIndexto=t;
        node[f].nodeIndex.push_back(t);
        node[t].nodeIndex.push_back(f);
        kd++;
    }
    for(j=0;j<Length;j++)
    {
        f=Length*(Higth-1)+j;
        t=j;
        edge[kd].edgeIndexfrom=f;	
        edge[kd].edgeIndexto=t;
        node[f].nodeIndex.push_back(t);
        node[t].nodeIndex.push_back(f);
        kd++;
    }
    //////////////////////////////////////////////找到每个邻居的r范围内的邻居构成一个集合
    for(i=0;i<nodeNumber;i++)///////对于每个节点城市进行初始化
    {
        left.push_back(i);
        distence=int(float(A)*pow(node[i].k,0.5));
        node[i].wide.resize(distence);/////////////定义一个二维的向量！！
    }
    ////储存过程便于进行网络的构造
    for(i=0;i<nodeNumber;i++)
    {	
        for(nindex2=node[i].nodeIndex.begin();nindex2!=node[i].nodeIndex.begin()+m0;++nindex2)
        {
            node[i].wide[0].push_back(*nindex2);
            node[i].chain.push_back(*nindex2);
        }//只能使用边查重边储存的方法进行储存和排列
        distence=int(A*pow(node[i].k,0.5));
        for(j=1;j<distence;j++)
        {
            for(nindex2=node[i].wide[j-1].begin();nindex2!=node[i].wide[j-1].end();++nindex2) 	
            {
                for(nindex=node[*nindex2].nodeIndex.begin();nindex!=node[*nindex2].nodeIndex.begin()+m0;++nindex)
                {
                    if(find(node[i].chain.begin(), node[i].chain.end(), *nindex)==node[i].chain.end() && *nindex!=i)
                    {
                        node[i].wide[j].push_back(*nindex);
                        node[i].chain.push_back(*nindex);
                    }
                }			
            }
        }
    }
    //////////////////////////////////////////按照度分布进行连接过程///注意要避免重连现象的出现
    do{
        n=rand_int(left.size());
        j=*(left.begin()+n);	
        for(nindex2=node[j].chain.begin()+m0;node[j].nodeIndex.size()<node[j].k&&nindex2!=node[j].chain.end();++nindex2)
        {	
            t=*nindex2;		
            if(node[t].nodeIndex.size()<node[t].k && find(node[j].nodeIndex.begin(), node[j].nodeIndex.end(), t)==node[j].nodeIndex.end())////避免出现自连接现象出现
            {				
                edge[kd].edgeIndexfrom=j;	
                edge[kd].edgeIndexto=t;
                node[j].nodeIndex.push_back(t);
                node[t].nodeIndex.push_back(j);
                kd++;
            }
        }
        left.erase(remove(left.begin(), left.end(), j), left.end());
    }while(left.size()>0);
    edgeNumber=kd;
    for(i=0;i<nodeNumber;i++)
    {node[i].k=node[i].nodeIndex.size();}///实际上有时并不是所有的节点的度都满足原来的序列需要有足够符合条件


    member_number=n0*edgeNumber*2;
    member=new Host[member_number];

    max=node[0].nodeIndex.size();
    for(i=0;i<nodeNumber;i++)	
    {
        if(node[i].nodeIndex.size()>max)
            max=node[i].nodeIndex.size();
    }
    for(i=0;i<nodeNumber;i++)
    {node[i].chain.swap(node[i].free);}
    delete [] edge;
}
/////////////////初始化状态函数//////////////////////////////////////
void Net::inState1()
{
    int i,j;
    iptime=new long double[Round];
    infectpro=new long double[Round];
    nodepro=new long double[max+1];
    nodek=new long double[max+1];/////具有与下标相同度的节点的数目
    stat=new int[member_number];
    for(i=0;i<nodeNumber;i++)
    {
        (node[i].mp)=new float[node[i].k];
        (node[i].s)=new float[node[i].k];
    }
}
void Net::inState(int seed1)
{
    int i,j,l;

    init_genrand64(seed1);
    ipmax=0;
    infectmax=0;
    judge=0;
    round=0;
    for(i=0;i<max+1;i++)
    {
        nodepro[i]=0;
        nodek[i]=0;	
    }
    for(j=0;j<Round;j++)
    {
        iptime[j]=0;
        infectpro[j]=0;
    }
    l=0;
    for(i=0;i<nodeNumber;i++)
    {
        for(j=0;j<node[i].k*n0;j++)
        {
            member[l].label=i;///刚开始设定每个邻居所在的城市标签
            member[l].state=0;///默认都是未感染态
            node[i].group.push_back(l);///每个城市的成员标签储存起来
            l++;
        }
    }
    for(i=0;i<nodeNumber;i++)
    {
        j=node[i].k;
        nodek[j]+=1.0;
        node[i].emer=0;
    }///先从一个城市开始扩散
    i=rand_int(member_number);
    member[i].state=1;//随机选择一个城市中的一个随机的个体作为感染源进行传播	
}
//////////传播过程的研究先进行迁移然后进行感染过程/////////////////////////////
void Net::Spread(int r1, float rate, float v)
{
    float p,sn,ex,lx;
    int i,j,l,mf0,ti,n,distence,xx;
    float mf,mt;
    ////////////////////////////////第一阶段迁移///////////////////////////////////////////
    for(i=0;i<member_number;i++)
    {
        stat[i]=0;
    }
    for(i=0;i<nodeNumber;i++)
    {
        node[i].emer=0;
        number[i]=i;
        for(j=0;j<node[i].k;j++)
        {
            node[i].mp[j]=0;
            node[i].s[j]=0;
        }
    }
    //////迁移过程包含两个子过程
    shuffle(number, nodeNumber);
    for(xx=0;xx<nodeNumber;xx++)
    {
        i=number[xx];
        if(node[i].group.size()>0)////如果城市中人口数目等于0，那么就不能向外进行迁移活动了
        {	
            sn=0;
            distence=int(float(A)*pow(node[i].k,0.5));
            n=0;
            for(nindex=node[i].nodeIndex.begin();nindex!=node[i].nodeIndex.end();++nindex)
            {////计算邻居城市的人口数目,markov过程
                sn+=float(node[*nindex].group.size());
                for(j=0;j<distence;j++)
                {
                    if(find(node[i].wide[j].begin(), node[i].wide[j].end(), *nindex)!=node[i].wide[j].end())
                        l=j;
                    else ;
                }
                for(j=0;j<l+1;j++)
                {
                    for(nindex2=node[i].wide[j].begin();nindex2!=node[i].wide[j].end();++nindex2)
                        node[i].s[n]+=node[*nindex2].group.size();
                }				
                node[i].s[n]-=float(node[*nindex].group.size());
                n++;
            }	
            if(sn!=0)////如果所有邻居城市中的成员数目和为0，那么就就不进行下面的迁移过程了，节省计算量
            {
                mf=float(node[i].group.size());///记录本城市的人口数目
                lx=0;
                j=0;
                for(nindex=node[i].nodeIndex.begin();nindex!=node[i].nodeIndex.end();++nindex)
                {	
                    mt=float(node[*nindex].group.size());
                    node[i].mp[j]=(mf*mt)/((mf+mt+node[i].s[j])*(mf+node[i].s[j]));
                    lx+=node[i].mp[j];
                    j++;
                }
            } 
            //////迁移过程开始
            if(lx>0)
            {
                for(j=0;j<node[i].k;j++)
                {node[i].mp[j]=node[i].mp[j]/lx;}

                ti=int(node[i].group.size()*v);	////v代表的是迁移率，代表的是整个系统的活性
                for(l=0;l<ti;l++)
                {
                    p=genrand64_real3();
                    ex=0;
                    for(j=0;j<node[i].k;j++)///找到迁移城市
                    {
                        if((p>ex)&&(p<=ex+node[i].mp[j]))//关键过程首先是添加然后是除名过程
                        {
                            mf0=rand_int(node[i].group.size());//本城市随机找一个个体进行迁移
                            nindex=node[i].group.begin()+mf0;//这儿应该有点儿问题，需要注意
                            mf0=*nindex;///要进行迁移的个体
                            nindex2=node[i].nodeIndex.begin()+j;
                            member[mf0].label=*nindex2;///搬迁后重新赋值
                            node[*nindex2].group.push_back(mf0);//////添加过程先添加后删除
                            node[i].group.erase(remove(node[i].group.begin(),node[i].group.end(),mf0),node[i].group.end());
                        }else ex+=node[i].mp[j];
                    }
                }
            }
        } 	
    }
    /////////////////////////////////第二阶段感染过程////////////////////////////////////////
    for(i=0;i<member_number;i++)
    {
        if(member[i].state==1)
        {
            l=member[i].label;
            node[l].emer++;
        }
    }
    for(i=0;i<nodeNumber;i++)///内部感染过程跟遍历顺序就已经没有关系了感染过程
    {//cout<<"i "<<i<<endl;
        if(node[i].emer>0)//得考虑emer的更新方式和步骤，感染和恢复完之后在进行计算！！！！
        {////这儿已经说明size是大于零的不需要在说明了
            for(j=0;j<fre*node[i].emer;j++)
            {
                l=rand_int(node[i].group.size());
                n=*(node[i].group.begin()+l);
                if(member[n].state==0 && stat[n]==0)
                {
                    p=genrand64_real3();
                    if(p<rate) stat[n]=1;
                }  
            }
        } 
    } 
    for(i=0;i<member_number;i++)////恢复过程
    {
        if(member[i].state==1) 
        {
            p=genrand64_real3();
            if(p<re) stat[i]=0;////康复率的问题要注意
            else stat[i]=1;
        } else ;
    }
    for(i=0;i<member_number;i++)
    {
        member[i].state=stat[i];
    }
    ///////////////////////////////////////////////////////////////////////数据记录过程
    for(i=0;i<nodeNumber;i++)
    {node[i].emer=0;}

    for(i=0;i<member_number;i++)
    {
        if(member[i].state==1)
        {
            iptime[r1]+=1.0;///////计算感染者数目的比例
            l=member[i].label;
            node[l].emer++;
        }
    }
    for(i=0;i<nodeNumber;i++)
    {
        if(node[i].emer>0)
            infectpro[r1]+=1.0;//////计算感染城市数所占的比例
    }
    iptime[r1]=iptime[r1]/double(member_number);
    infectpro[r1]=infectpro[r1]/double(nodeNumber); 
    //////////////////////////////////////////////////////////////////////
    if(r1>199)
    {		
        ipmax=iptime[r1]-iptime[r1-1];
        infectmax=infectpro[r1]-infectpro[r1-1];	
        if(ipmax<0.0000001&&infectmax<0.000002)
        {
            round=r1;
            for(i=0;i<nodeNumber;i++)
            {
                if(node[i].emer>0)
                {
                    l=node[i].k;
                    nodepro[l]+=1.0;
                }
            }
            for(i=0;i<max+1;i++)
            {
                if(nodepro[i]>0)
                    nodepro[i]=nodepro[i]/nodek[i];
                else nodepro[i]=0;
            }
            ipro=iptime[r1];
            infect=infectpro[r1];
            for(i=0;i<=nodeNumber;i++)//进行下一回合时个体重新进行安排因为原来的迁移已经具有很深影响了
            {
                for(nindex=node[i].group.begin();nindex!=node[i].group.end();)
                    nindex=node[i].group.erase(nindex);
            }
            judge=1;
        }
    }
}
//（因为有20个种子）一系列单独的数值返回函数，将各种数值单独返回(因为一个函数只能返回一个数值)传输中介//////////////////////
void Ensemble::Print()
{
    int i,j,l;
    float rate0;
    float tep=0.01;
    int size=60;
    char filename[3][size];
    FILE *fp[3];
    long double ipro0,infect0;
    long double *nodepro0;///////参数最后的数值依赖于感染率和n0的大小
    int mink;
    float vv,vvf;

    Build();	
    mink=net[0].max;
    for(i=1;i<seedNumber;i++)
    {
        if(net[i].max<mink)
            mink=net[i].max;
    }/////实际上应该寻找都最小的数值，因为寻找最大的的话，有些可能并不存在
    nodepro0=new long double[mink+1];

    for(i=0;i<seedNumber;i++)
    {
        net[i].inState1();
    }
    rate0=0.6;
    vvf=0.0016;
    sprintf(filename[0],"functionvrate%4.2fiprovvf%f.txt",rate0,vvf);
    sprintf(filename[1],"functionvrate%4.2finfectpro%f.txt",rate0,vvf);
    for(i=0;i<2;i++)
    {fp[i]=fopen(filename[i], "a");}
    for(vv=vvf;vv<vvf+0.0005;vv+=0.00002)
    {
        //sprintf(filename[2],"functionv%frate%4.2fnodepro.txt",vv,rate0);			
        //in[2].open(filename[2]);			
        for(i=0;i<seedNumber;i++)
        {net[i].inState(i);}
        for(i=0;i<seedNumber;i++)				
        {
            for(j=0;j<Round&&net[i].judge==0;j++)
            {cout<<j<<endl;net[i].Spread(j,rate0,vv);}
        }

        ipro0=0;
        infect0=0;
        for(j=0;j<mink+1;j++) nodepro0[j]=0;

        for(i=0;i<seedNumber;i++)							               		
        {							
            ipro0+=net[i].ipro;
            infect0+=net[i].infect;
            for(j=0;j<mink+1;j++)
            {nodepro0[j]+=net[i].nodepro[j];}////}
    }
    ipro0=ipro0/double(seedNumber);
    infect0=infect0/double(seedNumber);		
    for(j=0;j<mink+1;j++)
    {nodepro0[j]=nodepro0[j]/double(seedNumber);}////}	
    fprintf(fp[0],"%f %Lf\n",rate0,ipro0);
    fprintf(fp[1],"%f %Lf\n",rate0,infect0);			

    //for(j=0;j<mink+1;j++)
    //in[2]<<rate0<<" "<<j<<" "<<nodepro0[j]<<"\n";
    //in[2].close();	
    }
for(i=0;i<2;i++)
{fclose(fp[i]);}
}
//////////////////////////////////（因为有20个种子）一系列单独的数值返回函数，将各种数值单独返回(因为一个函数只能返回一个数值)传输中介///主函数调用/////////////////////////////////////////////////////
int main()
{
    Ensemble en;
    en.Print();
    return 0;
}
